import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-main',
  templateUrl: './property-main.component.html',
  styleUrls: ['./property-main.component.css']
})
export class PropertyMainComponent implements OnInit {

  role = sessionStorage['type']
  userName = sessionStorage['firstName']
  urlData = this.router.url
  selectedItem
  groups = [
    
  ]

  constructor(private router:Router) {
    
   }

  ngOnInit(): void {
    

    if(this.role == 'customer')
    {
      this.groups.push({
        name : "Properties",
        link : "/home/property/property-list"
      })
    }
    else
    {
      this.groups.push({
        name : "Properties",
        link : "/home/property/property-list"
      },
      {
        name : "Add Property",
        link : "/home/property/add-property"
      })
    }

    this.loadUrl()
  }

  loadUrl()
  {
    if(this.urlData == "/home/property/property-list")
    {
      this.selectedItem = this.groups[0]
    }
    else if(this.urlData == "/home/property/add-property")
    {
      this.selectedItem = this.groups[1]
    }
  }

  listClick(event, newValue) {
    this.selectedItem = newValue;
    this.router.navigate([`${newValue.link}`.toString()])
  }

}
