import { ToastrService } from 'ngx-toastr';
import { PropertyService } from './../property.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.css']
})
export class PropertyDetailsComponent implements OnInit {
  property = []

  role = sessionStorage['type']

  propFeedback = {
    "feedback" : null,
    "ratings" : null
    } 

  constructor(private modalService:NgbModal,private PropertyService:PropertyService,private toastr:ToastrService) { }

  ngOnInit(): void {
    
  }

  closeDismiss()
  {
    this.modalService.dismissAll()
  }

  postFeedback()
  {
    this.PropertyService.postFeedback(this.property['propertyId'],this.propFeedback).subscribe(res=>{
      if(res['status']=='success')
      {
        this.toastr.success("Feedback Sent!")
      }
      else{
        this.toastr.success("Failed!")
      }
    })
    
  }

}
