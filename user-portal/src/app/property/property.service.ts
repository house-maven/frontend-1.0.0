import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  url = "http://34.230.74.245:3000/"

  userUrl = "http://34.230.74.245:4000/"
  
  httpOption = {
    headers : new HttpHeaders(
      {
        token :sessionStorage['token']
      }
    )
  }

  constructor(private HttpClient:HttpClient) { }

  getProperties()
  {
    return this.HttpClient.get((sessionStorage['type']=="customer"?this.url:this.userUrl)+"properties",this.httpOption)
  }

  getPropertyRequirement()
  {
    return this.HttpClient.get(this.url+"property_requirement",this.httpOption)
  }

  getPropertyRequirementById(id)
  {
    return this.HttpClient.get(this.userUrl+`property_requirement/${id}`,this.httpOption)
  }

  editPropertyRequirement(id,data)
  {
    console.log(this.userUrl+"property_requirement/"+id);
    return this.HttpClient.put(this.userUrl+"property_requirement/"+id,data,this.httpOption)
  }
  postPropertyRequirement(data)
  {
    console.log(data)
    return this.HttpClient.post(this.userUrl+"property_requirement/ ",data,this.httpOption);
  }

  postProperty(propertyData)
  {
    const body = new FormData();
    body.append('images',propertyData['images'])
    console.log(propertyData['images']);

    propertyData['images'] = body

    return this.HttpClient.post(this.userUrl+"properties",propertyData,this.httpOption)
  }

  postPropertyImages(id,file)
  {
    const body = new FormData();
    body.append('images',file)
    console.log("service.ts file : ");
    console.log(body)

    return this.HttpClient.post(this.userUrl+`properties/upload-image/${id}`,body,this.httpOption)
  }

  loadProperty(propId)
  {
    return this.HttpClient.get(this.userUrl+`properties/${propId}`,this.httpOption)
  }

  editProperty(id,data)
  {
    return this.HttpClient.put(this.userUrl+`properties/${id}`,data,this.httpOption)
  }

  postFeedback(id,body)
  {
    return this.HttpClient.post(this.userUrl+`ratings_and_feedback/${id}`,body,this.httpOption)
  }
}
