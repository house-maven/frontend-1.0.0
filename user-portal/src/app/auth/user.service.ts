import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate{
  url = 'http://34.230.74.245:4000'

  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log("AWD")
    if (sessionStorage['token']) {
    return true
    }
    this.router.navigate(['/auth/signin'])
    return false 
    }


  login(email: string, password: string) {
    const body = {
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/signin', body)
  }

  signup(firstName: string, lastName: string, email: string, type: string, password: string) {
    const body = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      type: type,
      password: password
    }

    return this.httpClient.post(this.url + '/signup', body)
  }
  
}
