import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  title = "Signup"
  firstName: string;
  lastName: string;
  email: string;
  type: string;
  password: string;

  titles = [
    { value: 'agent', text: 'Agent' },
    { value: 'owner', text: 'Owner' },
    { value: 'customer', text: 'Customer' },
  ];
  
  modifiedTitle = {
    title: 'agent',
  };

  constructor(
    private userService: UserService,
    private toastrService: ToastrService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  signup(){
    console.log(this.modifiedTitle.title)
    if(this.firstName == null || this.lastName == null || this.email == null || this.password == null)
    {
      this.toastrService.warning("Please fill all the fields")
    }else{
      this.userService.signup(this.firstName, this.lastName, this.email, this.modifiedTitle.title, this.password).subscribe(response => {
        if(response['status']=="success")
        {
          // window.alert("User signed up successful");
          this.toastrService.success("User signed up successful");
          this.router.navigate(['/auth/signin'])
        }else{
          console.log(response['error'])
          this.toastrService.error(response['error']);
        }
      })
    }
  }
}
