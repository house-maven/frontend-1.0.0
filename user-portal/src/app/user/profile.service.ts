import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  url = "http://34.230.74.245:4000/"

  httpOption = {
    headers : new HttpHeaders({
      token : sessionStorage['token']
    })
  };

  constructor(private httpClient:HttpClient) { }

  getProfile()
  {
    return this.httpClient.get(this.url,this.httpOption)
  }

  editProfile(body)
  {
    return this.httpClient.put(this.url+"edit",body,this.httpOption);
  }
}
