import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-main',
  templateUrl: './user-main.component.html',
  styleUrls: ['./user-main.component.css']
})
export class UserMainComponent implements OnInit {

  userName = sessionStorage['firstName']
  urlData = this.router.url
  selectedItem
  groups = [
    {
      name : "Profile",
      link : "/home/user/profile"
    },
    {
      name : "Edit Profile",
      link : "/home/user/edit-profile"
    }
  ]
  constructor(private router:Router) { }

  ngOnInit(): void {
    if(this.urlData == "/home/user/profile")
    {
      this.selectedItem = this.groups[0]
    }
    else{
      this.selectedItem = this.groups[1]
    }
  }

  listClick(event, newValue) {
    this.selectedItem = newValue;
    this.router.navigate([`${newValue.link}`.toString()])
  }
}
