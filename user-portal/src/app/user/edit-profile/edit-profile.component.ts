import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProfileComponent } from './../profile/profile.component';
import { ProfileService } from './../profile.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  profileData = []

  constructor(private ProfileService:ProfileService,private toastr:ToastrService,private Router:Router) { }

  ngOnInit(): void {
    this.loadProfile()
  }

  loadProfile()
  {
    this.ProfileService.getProfile().subscribe(res=>{
      if(res['status'] == "success")
      {
        this.profileData = res['data'][0]
        console.log(res['data'])
      }
    })
  }

  onEditProfile()
  {
    if(this.profileData['phone']==null)
    {
      this.toastr.warning("Enter Phone Number!");
    }
    else{
      this.ProfileService.editProfile(this.profileData).subscribe((res)=>{
        if(res['status'] == "success")
        {
            this.toastr.success("Profile Updated!")
            this.Router.navigate(['/home/user/profile'])
        }
        else{
          this.toastr.error("Failed!")
        }
      })
    }
  }

}
