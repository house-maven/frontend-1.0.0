import { UserMainComponent } from './user-main/user-main.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ProfileComponent } from './profile/profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path : "",
      component : UserMainComponent,
      children : [
        {
          path : "profile",
          component : ProfileComponent
        },
        {
          path : "edit-profile",
          component : EditProfileComponent
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
