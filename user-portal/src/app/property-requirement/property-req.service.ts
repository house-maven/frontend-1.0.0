import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PropertyReqService {

  Adminurl = "http://34.230.74.245:3000/"
  Userurl = "http://34.230.74.245:4000/"

  constructor(private HttpClient:HttpClient) { }

  getPropertyRequirement()
  {
    const httpOption = {
      headers : new HttpHeaders(
        {
          token :sessionStorage['token']
        }
      )
    }
    return this.HttpClient.get((sessionStorage['type']=="customer"?this.Userurl:this.Adminurl)+"property_requirement",httpOption)
  }
}
