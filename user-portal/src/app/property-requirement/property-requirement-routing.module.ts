import { PropertyReqMainComponent } from './property-req-main/property-req-main.component';
import { AddPropertyRequirementComponent } from './add-property-requirement/add-property-requirement.component';
import { EditPropertyRequirementComponent } from './edit-property-requirement/edit-property-requirement.component';
import { PropertyRequirementListComponent } from './property-requirement-list/property-requirement-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path : "",
    component: PropertyReqMainComponent,
    children : [
      {
        path : "property-requirement-list",
        component : PropertyRequirementListComponent
      },
      {
        path : "edit-property-requirement",
        component : EditPropertyRequirementComponent
      },
      {
        path : "add-property-requirement",
        component : AddPropertyRequirementComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRequirementRoutingModule { }
