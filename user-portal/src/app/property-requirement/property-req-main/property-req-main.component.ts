import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-req-main',
  templateUrl: './property-req-main.component.html',
  styleUrls: ['./property-req-main.component.css']
})
export class PropertyReqMainComponent implements OnInit {

  userName = sessionStorage['firstName']
  urlData = this.router.url
  selectedItem
  groups = [
    {
      name : "Properties Requirement",
      link : "/home/property-requirement/property-requirement-list"
    },
    {
      name : "Add Property Requirement",
      link : "/home/property-requirement/add-property-requirement"
    }
  ]

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.loadUrl();
  }

  loadUrl()
  {
    if(this.urlData == "/home/property-requirement/property-requirement-list")
    {
      this.selectedItem = this.groups[0]
    }
    else if(this.urlData == "/home/property-requirement/add-property-requirement")
    {
      this.selectedItem = this.groups[1]
    }
  }

  listClick(event, newValue) {
    this.selectedItem = newValue;
    this.router.navigate([`${newValue.link}`.toString()])
  }

}
