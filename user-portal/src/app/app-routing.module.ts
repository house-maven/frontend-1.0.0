import { UserService } from './auth/user.service';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'',redirectTo:'/home/user/profile',pathMatch:'full'},
  {path:'auth',loadChildren:()=>import('./auth/auth.module').then(m=>m.AuthModule)},
  {
    path : "home",
    component : HomeComponent,
    canActivate : [UserService],
    children : [
      {
        path : "user",
        loadChildren : ()=>import('./user/user.module').then(m=>m.UserModule)
      },
      {
        path : "property",
        loadChildren : ()=>import('./property/property.module').then(m=>m.PropertyModule)
      },
      {
        path : "property-requirement",
        loadChildren : ()=>import('./property-requirement/property-requirement.module').then(m=>m.PropertyRequirementModule)
      }
    ]
  },
  {
    path :"home",
    redirectTo : "/home/user/profile",
    pathMatch : "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
